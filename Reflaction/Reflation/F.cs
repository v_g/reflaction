﻿using System;

namespace Reflaction
{
    [Serializable]
    public class F
    {
        [NonSerialized]
        public string Description = "It's a description";

        public int Field_1 { get; set; }
        public int Field_2 { get; set; }
        public int Field_3 { get; set; }
        public int Field_4 { get; set; }
        public int Field_5 { get; set; }
    }
}