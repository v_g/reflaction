﻿namespace Reflaction
{
    class Program
    {
        static void Main(string[] args)
        {
            var f = new F()
            {
                Field_1 = 1,
                Field_2 = 2,
                Field_3 = 3,
                Field_4 = 4,
                Field_5 = 5
            };

            string csv = "{\"Field_1\":1,\"Field_2\":2,\"Field_3\":3,\"Field_4\":4,\"Field_5\":5}";

            // Custom serialization
            Serializer.SerializeFromObjectToCSV(Serializer.SerializationType.Custom, f);

            // Newtonsoft serialization
            Serializer.SerializeFromObjectToCSV(Serializer.SerializationType.Newtonsoft, f);

            // Custom deserialization
            Serializer.DeserializeFromCSVtoObject(Serializer.SerializationType.Custom, csv);

            // Newtonsoft deserialization
            Serializer.DeserializeFromCSVtoObject(Serializer.SerializationType.Newtonsoft, csv);
        }
    }
}
