﻿using System;
using System.Text;
using System.Reflection;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using System.Linq;

namespace Reflaction
{
    class Serializer
    {
        private static StringBuilder jsonString = new StringBuilder();
        private static int counter = 0;
        private static string pattern = "\"(?<fieldName>.+?)\":(?<fieldValue>\\d+)";
        private static string assemblyFile = Environment.CurrentDirectory + "\\Reflaction.dll";        

        public static void SerializeFromObjectToCSV(SerializationType serializationType, object obj)
        {
            Stopwatch sw = new Stopwatch();

            Console.WriteLine();
            Console.WriteLine("Serialization from Object to Json start: " + DateTime.Now.ToLongTimeString());
            Console.WriteLine();

            jsonString.Clear();
            counter = 0;

            sw.Start();

            switch (serializationType)
            {
                case SerializationType.Custom:
                    {
                        Console.WriteLine("Custom json serialization");
                        Console.WriteLine();

                        for (int i = 0; i < 100; i++)
                        {
                            Type type = null;

                            if (obj is F)
                            {
                                type = obj.GetType();
                            }

                            PropertyInfo[] properties = type.GetProperties();
                            jsonString.Append("{");

                            for (int j = 0; j < properties.Length; j++)
                            {
                                jsonString.Append("\"" + properties[j].Name + "\"" + ":" + properties[j].GetValue(obj));

                                if (j != (properties.Length - 1))
                                {
                                    jsonString.Append(",");
                                }
                            }

                            jsonString.Append("}");

                            counter++;
                        }

                        break;
                    }
                case SerializationType.Newtonsoft:
                    {
                        Console.WriteLine("Newtonsoft json serialization");
                        Console.WriteLine();

                        for (int i = 0; i < 100; i++)
                        {
                            jsonString.Append(JsonConvert.SerializeObject(obj));
                            counter++;
                        }

                        break;
                    }
                default:
                    {
                        Console.WriteLine("Unknown type of serialization");
                        break;
                    }
            }

            sw.Stop();

            Console.WriteLine("Serialization from Object to Json finish: " + DateTime.Now.ToLongTimeString());
            Console.WriteLine();
            Console.WriteLine("Total serialization time: " + (sw.ElapsedMilliseconds).ToString() + " milliseconds");
            Console.WriteLine();
            Console.WriteLine("Microsoft Visual Studio Community 2019");
            Console.WriteLine();
            Console.WriteLine("Number of iterations: " + counter);
            Console.WriteLine();

            Console.ReadKey();

            sw.Start();
            Console.WriteLine("Resulting string: " + jsonString);
            sw.Stop();

            Console.WriteLine();
            Console.WriteLine("Total time of text output to the console: " + (sw.ElapsedMilliseconds).ToString() + " milliseconds");

            Console.ReadKey();
        }

        public static void DeserializeFromCSVtoObject(SerializationType serializationType, string csv)
        {
            Stopwatch sw = new Stopwatch();

            Console.WriteLine();
            Console.WriteLine("Deserialization from Json to Object start: " + DateTime.Now.ToLongTimeString());
            Console.WriteLine();

            jsonString.Clear();
            counter = 0;

            Assembly assembly = Assembly.LoadFrom(assemblyFile);
            Type typeF = assembly.GetType("Reflaction.F", true, true);

            sw.Start();

            switch (serializationType)
            {
                case SerializationType.Custom:
                    {
                        Console.WriteLine("Custom json deserialization");
                        Console.WriteLine();

                        for (int i = 0; i < 100; i++)
                        {
                            string[] fieldsName = Regex.Matches(csv, pattern, RegexOptions.IgnoreCase).OfType<Match>()
                                                       .Select(field => field.Groups["fieldName"].Value).ToArray();

                            string[] fieldsValue = Regex.Matches(csv, pattern, RegexOptions.IgnoreCase).OfType<Match>()
                                                        .Select(field => field.Groups["fieldValue"].Value).ToArray();

                            PropertyInfo[] properties = typeF.GetProperties();
                            F f = (F)Activator.CreateInstance(typeF);

                            for (int j = 0; j < 5; j++)
                            {
                                PropertyInfo property = typeF.GetProperty(fieldsName[j]);
                                property.SetValue(f, Int32.Parse(fieldsValue[j]));
                            }

                            counter++;
                        }

                        break;
                    }
                case SerializationType.Newtonsoft:
                    {
                        Console.WriteLine("Newtonsoft json deserialization");
                        Console.WriteLine();

                        for (int i = 0; i < 100; i++)
                        {
                            JsonConvert.DeserializeObject(csv, typeF);
                            counter++;
                        }
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Unknown type of deserialization");
                        break;
                    }
            }

            sw.Stop();

            Console.WriteLine("Deserialization from Json to Object finish: " + DateTime.Now.ToLongTimeString());
            Console.WriteLine();

            Console.WriteLine("Total deserialization time: " + (sw.ElapsedMilliseconds).ToString() + " milliseconds");
            Console.WriteLine();
            Console.WriteLine("Microsoft Visual Studio Community 2019");
            Console.WriteLine();
            Console.WriteLine("Number of iterations: " + counter);
            Console.WriteLine();

            Console.ReadKey();
        }

        public enum SerializationType
        {
            Custom,
            Newtonsoft
        }
    }
}
